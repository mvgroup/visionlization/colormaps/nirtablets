import numpy as np
import matplotlib as mtply
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
import colour as c


def get_n_colors(cmap,n):

    assert isinstance(cmap, mtply.colors.LinearSegmentedColormap) or isinstance(cmap, mtply.colors.ListedColormap) or isinstance(cmap, list), "cmap needs to be of type matplotlib.colors.ListedColormap or list"

    if isinstance(cmap, list):

        cmap = ListedColormap(cmap)

    sampling = np.linspace(0,1,n)
    cmap_n = cmap(sampling)

    return cmap_n[:,:3] 



# Definition of functions
def get_lightness(rgb):
    return c.convert(rgb, "sRGB", "CAM02UCS")[:,0]

def plot_lightness(rgb):
    L = get_lightness(rgb)
    x = np.linspace(0,1, L.shape[0])
    f, ax = plt.subplots(1, figsize=(8,4))
    ax.scatter(x, L, c=x, cmap=ListedColormap(rgb), s=20)
    ax.set_ylabel('Lightness (CIECAM02)')
    ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    ax.set_title('Ligthness linearity of colormap')

    return f, ax
    # plt.show()

def plot_lightness_derivative(rgb):
    L = get_lightness(rgb)
    x = np.linspace(0,1, L.shape[0]) 
    deriv = np.diff(L) / np.diff(x)
    amp = deriv.max() - deriv.min()
    y_lims = (deriv.min() - amp, deriv.max() + amp)
    f, ax = plt.subplots(1, figsize=(8,4))
    ax.scatter(x[1:], deriv, c=x[1:], cmap=ListedColormap(rgb[1:,:]))
    ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    ax.set_ylabel('Lightness derivative')
    ax.set_title('Ligthness linearity of colormap')
    ax.set_ylim(y_lims)

    return f, ax
    # plt.show()

def pyramid(n):
    r = np.linspace(0,255, n)
    d = np.minimum(r,r[::-1])
    return np.minimum.outer(d,d)

def plot_pyramids(rgb):
    f, axes = plt.subplots(1,3, figsize=(15,5))
    axes[0].imshow(pyramid(50), cmap=plt.cm.get_cmap('magma'))
    axes[0].set_title('Perceptually uniform (Magma)')
    axes[1].imshow(pyramid(50), cmap=ListedColormap(rgb))
    axes[1].set_title('Selected colormap')
    axes[2].imshow(pyramid(50), cmap='jet')
    axes[2].set_title('Perceptually non-uniform (Jet)')
    for ax in axes:
        ax.axis("off")

    return f, axes
    # plt.show()